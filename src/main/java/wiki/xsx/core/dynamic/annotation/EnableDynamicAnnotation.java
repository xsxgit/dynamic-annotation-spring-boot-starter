package wiki.xsx.core.dynamic.annotation;

import java.lang.annotation.*;

/**
 * 动态注解标记
 * @author xsx
 * @date 2020/6/16
 * @since 1.8
 * <p>
 * Copyright (c) 2020 xsx All Rights Reserved.
 * dynamic-annotation-spring-boot-starter is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 * </p>
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableDynamicAnnotation {

    /**
     * 待扫描的注解
     * @return 返回待扫描的注解类型
     */
    Class<? extends Annotation>[] value();

    /**
     * 需要排除的类
     * @return 返回排除的类型
     */
    Class<?>[] exclude() default {};
}
