# dynamic-annotation-spring-boot-starter

<p align="center">
    <img src="https://img.shields.io/badge/JDK-1.8+-green.svg" />
    <img src="https://img.shields.io/badge/Current%20Version-v0.0.1-brightgreen" />
    <img src="https://img.shields.io/:License-MulanPSL-yellowgreen.svg" />
    <a href='https://gitee.com/xsxgit/dynamic-annotation-spring-boot-starter/stargazers'>
        <img src='https://gitee.com/xsxgit/dynamic-annotation-spring-boot-starter/badge/star.svg?theme=dark' alt='star' />
    </a>
</p>

#### 介绍
动态注解

#### 软件架构
仅依赖spring-boot-starter

#### 安装教程
方式一：添加maven依赖
```xml
<dependency>
    <groupId>wiki.xsx</groupId>
    <artifactId>dynamic-annotation-spring-boot-starter</artifactId>
    <version>0.0.1</version>
</dependency>
```
方式二：自行安装添加依赖
```cmd
mvn clean intall
```

#### 使用说明

1.  启动类添加注解 “@EnableDynamicAnnotation({XXXX.class})” ，并加入需要扫描的注解
```java
// 添加动态注解
@EnableDynamicAnnotation({Component.class})
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
```

2.  配置文件application.yml或环境变量中加入待替换的配置

方式一：
```yaml
my:
 name: xsx
```
方式二：
```
System.setProperty("my.name", "xsx");
```

3.  在需要动态替换的注解中使用 “${XXXX}” 形式的占位符
```java
@Slf4j
@Component("${my.name}")
public class MyService {

    public void myName() {
        log.info("调用myName");
        log.info("my.name = " + this.getClass().getAnnotation(Component.class).value());
    }
}
```

4. 测试调用效果：
```cmd
2020-06-22 16:34:45.733  INFO 24236 --- [           main] wiki.xsx.log.service.MyService           : 调用myName
2020-06-22 16:34:45.733  INFO 24236 --- [           main] wiki.xsx.log.service.MyService           : my.name = xsx
```

#### @EnableDynamicAnnotation注解说明
1. value：待扫描的注解
2. exclude：需要排除的类